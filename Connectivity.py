import mysql.connector
import configparser
from DebugLogs import DebugLogging


def mySQLConnection():
    try:

        config = configparser.ConfigParser()
        config.read('conf/config.properties')
        dbuser = config.get('mysql', 'user').replace("'", "")
        dbpassword = config.get('mysql', 'password').replace("'", "")
        dbhost = config.get('mysql', 'host').replace("'", "")
        db = config.get('mysql', 'database').replace("'", "")
        dbport = int(config.get('mysql', 'port').replace("'", ""))

        cnx = mysql.connector.MySQLConnection(user=dbuser, password=dbpassword,
                                              host=dbhost,
                                              database=db, port=dbport,
                              auth_plugin='mysql_native_password')

        return cnx

    except mysql.connector.Error as err:
        DebugLogging("Something went wrong: {}".format(err))
