# tracking-db-flask-docker

1. Extract DB Flask.rar or zip

2. Open main.py with any interpreter
3. Change "app.config['MYSQL_USER'] = 'root' " in line 14 to your mysql username
4. Change "app.config['MYSQL_HOST'] = '127.0.0.1'" in line 15 to your mysql host
5. Change "app.config['MYSQL_PASSWORD'] = 'mysql' "  in line  18 to your mysql password
6. Change "app.config['MYSQL_DB'] = 'ejes2'" in line 21 to  name of your db

Make sure you have installed these python modules 'flask' and 'flask-mysqldb'

