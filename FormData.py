from Connectivity import mySQLConnection
import mysql.connector
from DebugLogs import DebugLogging


def getCounterData():
    try:
        DBconnection = mySQLConnection()
        dbcursor = DBconnection.cursor()
        dailycountSQL = "select count(id) from deploys where date=CURDATE();"
        monthlyCountSQL = "select count(id) from deploys where Month (date)=MONTH(CURDATE());"
        YearlyCountSQL = "select count(id) from deploys where Year (date)=Year(CURDATE());"
        dbcursor.execute(dailycountSQL)
        dailycount = dbcursor.fetchone()
        dbcursor.execute(monthlyCountSQL)
        monthlycount = dbcursor.fetchone()
        dbcursor.execute(YearlyCountSQL)
        yearlycount = dbcursor.fetchone()
        DBconnection.commit()
        DBconnection.close()
        return (dailycount[0], monthlycount[0], yearlycount[0])

    except mysql.connector.Error as err:
        DebugLogging("Something went wrong while logging: {}".format(err))


def getFormData():
    try:
        DBconnection = mySQLConnection()
        dbcursor = DBconnection.cursor()
        sql = "select date,time,PDB,verTag,Hostname from deploys order by date desc"
        dbcursor.execute(sql)
        formresult = dbcursor.fetchall()
        DBconnection.commit()
        DBconnection.close()
        return formresult

    except mysql.connector.Error as err:
        DebugLogging("Something went wrong while logging: {}".format(err))


def getAutoScaleData():
    try:
        DBconnection = mySQLConnection()
        dbcursor = DBconnection.cursor()
        sql = "select autoscale_ip from  autoscaling"
        dbcursor.execute(sql)
        formresult = dbcursor.fetchall()
        DBconnection.commit()
        DBconnection.close()
        return formresult

    except mysql.connector.Error as err:
        DebugLogging("Something went wrong while logging: {}".format(err))
